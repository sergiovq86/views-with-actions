package com.estech.viewswithactions;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1, button2, button3;
    private ImageButton imageButton, editButton;
    private ToggleButton toggleButton;
    private Switch mSwitch;
    private CheckBox checkBox1, checkBox2, checkBox3;
    private EditText editText;
    private TextView textView;
    private RadioGroup radioGroup;
    private RadioButton radio1, radio2, radio3;
    private ProgressBar circular, horizontal, finiteBar;
    private int progressStatus;
    private Handler handler = new Handler();
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //inicializar vistas
        linearLayout = findViewById(R.id.linearlayout);
        textView = findViewById(R.id.text_content);
        radioGroup = findViewById(R.id.radiogroup);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        imageButton = findViewById(R.id.button4);

        finiteBar = findViewById(R.id.finiteBar);

        editText = findViewById(R.id.editText);
        editButton = findViewById(R.id.button_edit);

        mSwitch = findViewById(R.id.switch1);
        toggleButton = findViewById(R.id.toggleButton);

        checkBox1 = findViewById(R.id.checkBox);
        checkBox2 = findViewById(R.id.checkBox2);
        checkBox3 = findViewById(R.id.checkBox3);

        circular = findViewById(R.id.progressBar);
        horizontal = findViewById(R.id.progressBar2);

        //configuración clicks
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        editButton.setOnClickListener(this);
        checkBox1.setOnClickListener(this);
        checkBox2.setOnClickListener(this);
        checkBox3.setOnClickListener(this);
        mSwitch.setOnClickListener(this);
        toggleButton.setOnClickListener(this);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplication(), "ImageButton clicked!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton1) {
                    String text = "Radio 1 checked";
                    Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
                    textView.setText(text);
                    linearLayout.setBackgroundColor(getResources().getColor(R.color.blue_light));
                } else if (checkedId == R.id.radioButton2) {
                    String text = "Radio 2 checked";
                    Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
                    textView.setText(text);
                    linearLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    String text = "Radio 3 checked";
                    Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
                    textView.setText(text);
                    linearLayout.setBackgroundColor(getResources().getColor(R.color.red_light));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                imprimirMensaje("Button 1 clicked!!");
                comprobarRadioButton();
                break;

            case R.id.button2:
                imprimirMensaje("Button 2 clicked!!");
                break;

            case R.id.button3:
                imprimirMensaje("Starting progress bar");
                iniciarPogressBar();
                break;

            case R.id.switch1:
                textView.setText("ToggleButton: " + mSwitch.isChecked());

                if (mSwitch.isChecked()) {
                    circular.setVisibility(View.VISIBLE);
                } else {
                    circular.setVisibility(View.INVISIBLE);
                }
                break;

            case R.id.toggleButton:
                textView.setText("Switch: " + toggleButton.isChecked());

                if (toggleButton.isChecked()) {
                    horizontal.setVisibility(View.VISIBLE);
                } else {
                    horizontal.setVisibility(View.INVISIBLE);
                }
                break;

            case R.id.checkBox:
                textView.setText("Checkbox 1: " + checkBox1.isChecked());
                break;

            case R.id.checkBox2:
                textView.setText("Checkbox 2: " + checkBox2.isChecked());
                break;

            case R.id.checkBox3:
                textView.setText("Checkbox 3: " + checkBox3.isChecked());
                break;

            case R.id.button_edit:
                String text = editText.getText().toString();
                if (text.isEmpty()) {
                    text = "EditText is empty";
                }
                textView.setText(text);
                imprimirMensaje(text);
        }
    }

    private void imprimirMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    public void comprobarRadioButton() {
        String text;
        if (radioGroup.getCheckedRadioButtonId() == R.id.radioButton1) {
            text = "Radio 1 is checked";
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.radioButton2) {
            text = "Radio 2 is checked";
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.radioButton3) {
            text = "Radio 3 is checked";
        } else {
            text = "No Radio checked";
        }
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        textView.setText(text);
    }

    private void iniciarPogressBar() {
        progressStatus = 0;

        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;

                    handler.post(new Runnable() {
                        public void run() {
                            finiteBar.setProgress(progressStatus);
                            textView.setText(progressStatus + "/" + finiteBar.getMax());
                        }
                    });

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
